#!/bin/bash

#BSUB -P GEN119
#BSUB -W 0:05
#BSUB -J dask-affin
#BSUB -nnodes 1
#BSUB -alloc_flags "gpumps smt1"

set -o xtrace

work_dir=`mktemp -d $MEMBERWORK/gen119/dask-affin.XXXXXXXXXX`

# Single threaded BLAS with no binding
# NB: depending on the BLAS library NumPy is using, this may not be necessary
# (e.g., reference BLAS)
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1
export OPENBLAS_NUM_THREADS=1
export OMP_PROC_BIND=FALSE

dask-scheduler --interface ib0 --scheduler-file $work_dir/scheduler.json &
sched_pid=$!

# Noteworthy options:
# --bind rs: the (one) process launched in each resource set will be bound to
#            all cores of the resource set.
jsrun -n 2 -c 21 -g 0 -a 1 --bind packed:21 python -m distributed.cli.dask_worker --scheduler-file $work_dir/scheduler.json --nthreads 21 --nanny --death-timeout 60 --interface ib0 --local-directory $work_dir &

python test_affin.py -s $work_dir/scheduler.json

kill $sched_pid

wait # Wait for workers to stop cleanly
rm -rf $work_dir
