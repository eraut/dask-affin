# This is a simple script to determine thread affinity of a program calling
# Python

import os
import numpy as np

# Show NumPy configuration, such as which BLAS library it is configured to use
# NB: this information can be inaccurate.
# https://stackoverflow.com/questions/37184618/find-out-if-which-blas-library-is-used-by-numpy
#np.show_config()

print('Note OMP_NUM_THREADS=',os.getenv('OMP_NUM_THREADS'))
print('Affinity:',os.sched_getaffinity(0))
